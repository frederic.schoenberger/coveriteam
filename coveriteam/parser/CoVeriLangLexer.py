# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Generated from CoVeriLang.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2,")
        buf.write("\u018f\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write('\t\36\4\37\t\37\4 \t \4!\t!\4"\t"\4#\t#\4$\t$\4%\t%')
        buf.write("\4&\t&\4'\t'\4(\t(\4)\t)\4*\t*\4+\t+\3\2\3\2\3\2\3\2")
        buf.write("\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3")
        buf.write("\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n")
        buf.write("\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\r\3")
        buf.write("\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r")
        buf.write("\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23")
        buf.write("\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\27")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31")
        buf.write("\3\31\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34")
        buf.write("\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35")
        buf.write("\3\35\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37")
        buf.write("\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37")
        buf.write("\3\37\3\37\3\37\5\37\u0144\n\37\3 \3 \3 \3 \3 \3 \3 \3")
        buf.write(" \3 \5 \u014f\n \3!\3!\3!\3!\7!\u0155\n!\f!\16!\u0158")
        buf.write('\13!\3"\3"\7"\u015c\n"\f"\16"\u015f\13"\3"\3"')
        buf.write("\3#\3#\3#\7#\u0166\n#\f#\16#\u0169\13#\3$\3$\5$\u016d")
        buf.write("\n$\3%\3%\3&\3&\3'\3'\3(\5(\u0176\n(\3(\3(\3(\3(\3)")
        buf.write("\6)\u017d\n)\r)\16)\u017e\3)\3)\3*\3*\3+\3+\3+\3+\7+\u0189")
        buf.write("\n+\f+\16+\u018c\13+\3+\3+\2\2,\3\3\5\4\7\5\t\6\13\7\r")
        buf.write("\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!")
        buf.write("\22#\23%\24'\25)\26+\27-\30/\31\61\32\63\33\65\34\67")
        buf.write("\359\36;\37= ?!A\"C#E$G%I&K'M(O)Q*S+U,\3\2\t\3\2aa\3")
        buf.write('\2$$\3\2c|\3\2C\\\3\2\62;\4\2\13\13""\4\2\f\f\17\17')
        buf.write("\2\u019d\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2")
        buf.write("\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2")
        buf.write("\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2")
        buf.write("\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3")
        buf.write("\2\2\2\2%\3\2\2\2\2'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2")
        buf.write("-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3")
        buf.write("\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2")
        buf.write("?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2")
        buf.write("\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2")
        buf.write("\2\2S\3\2\2\2\2U\3\2\2\2\3W\3\2\2\2\5[\3\2\2\2\7]\3\2")
        buf.write("\2\2\t_\3\2\2\2\13a\3\2\2\2\rc\3\2\2\2\17e\3\2\2\2\21")
        buf.write("g\3\2\2\2\23n\3\2\2\2\25w\3\2\2\2\27~\3\2\2\2\31\u0080")
        buf.write("\3\2\2\2\33\u0095\3\2\2\2\35\u009e\3\2\2\2\37\u00a2\3")
        buf.write("\2\2\2!\u00a9\3\2\2\2#\u00b2\3\2\2\2%\u00b9\3\2\2\2'")
        buf.write("\u00be\3\2\2\2)\u00c5\3\2\2\2+\u00d6\3\2\2\2-\u00e7\3")
        buf.write("\2\2\2/\u00f0\3\2\2\2\61\u0108\3\2\2\2\63\u010a\3\2\2")
        buf.write("\2\65\u010e\3\2\2\2\67\u0119\3\2\2\29\u0123\3\2\2\2;\u012b")
        buf.write("\3\2\2\2=\u0143\3\2\2\2?\u014e\3\2\2\2A\u0150\3\2\2\2")
        buf.write("C\u0159\3\2\2\2E\u0162\3\2\2\2G\u016c\3\2\2\2I\u016e\3")
        buf.write("\2\2\2K\u0170\3\2\2\2M\u0172\3\2\2\2O\u0175\3\2\2\2Q\u017c")
        buf.write("\3\2\2\2S\u0182\3\2\2\2U\u0184\3\2\2\2WX\7h\2\2XY\7w\2")
        buf.write("\2YZ\7p\2\2Z\4\3\2\2\2[\\\7*\2\2\\\6\3\2\2\2]^\7+\2\2")
        buf.write("^\b\3\2\2\2_`\7}\2\2`\n\3\2\2\2ab\7\177\2\2b\f\3\2\2\2")
        buf.write("cd\7.\2\2d\16\3\2\2\2ef\7?\2\2f\20\3\2\2\2gh\7r\2\2hi")
        buf.write("\7t\2\2ij\7k\2\2jk\7p\2\2kl\7v\2\2lm\7*\2\2m\22\3\2\2")
        buf.write("\2no\7g\2\2op\7z\2\2pq\7g\2\2qr\7e\2\2rs\7w\2\2st\7v\2")
        buf.write("\2tu\7g\2\2uv\7*\2\2v\24\3\2\2\2wx\7t\2\2xy\7g\2\2yz\7")
        buf.write("v\2\2z{\7w\2\2{|\7t\2\2|}\7p\2\2}\26\3\2\2\2~\177\7<\2")
        buf.write("\2\177\30\3\2\2\2\u0080\u0081\7C\2\2\u0081\u0082\7e\2")
        buf.write("\2\u0082\u0083\7v\2\2\u0083\u0084\7q\2\2\u0084\u0085\7")
        buf.write("t\2\2\u0085\u0086\7H\2\2\u0086\u0087\7c\2\2\u0087\u0088")
        buf.write("\7e\2\2\u0088\u0089\7v\2\2\u0089\u008a\7q\2\2\u008a\u008b")
        buf.write("\7t\2\2\u008b\u008c\7{\2\2\u008c\u008d\7\60\2\2\u008d")
        buf.write("\u008e\7e\2\2\u008e\u008f\7t\2\2\u008f\u0090\7g\2\2\u0090")
        buf.write("\u0091\7c\2\2\u0091\u0092\7v\2\2\u0092\u0093\7g\2\2\u0093")
        buf.write("\u0094\7*\2\2\u0094\32\3\2\2\2\u0095\u0096\7U\2\2\u0096")
        buf.write("\u0097\7G\2\2\u0097\u0098\7S\2\2\u0098\u0099\7W\2\2\u0099")
        buf.write("\u009a\7G\2\2\u009a\u009b\7P\2\2\u009b\u009c\7E\2\2\u009c")
        buf.write("\u009d\7G\2\2\u009d\34\3\2\2\2\u009e\u009f\7K\2\2\u009f")
        buf.write("\u00a0\7V\2\2\u00a0\u00a1\7G\2\2\u00a1\36\3\2\2\2\u00a2")
        buf.write("\u00a3\7T\2\2\u00a3\u00a4\7G\2\2\u00a4\u00a5\7R\2\2\u00a5")
        buf.write("\u00a6\7G\2\2\u00a6\u00a7\7C\2\2\u00a7\u00a8\7V\2\2\u00a8")
        buf.write(" \3\2\2\2\u00a9\u00aa\7R\2\2\u00aa\u00ab\7C\2\2\u00ab")
        buf.write("\u00ac\7T\2\2\u00ac\u00ad\7C\2\2\u00ad\u00ae\7N\2\2\u00ae")
        buf.write("\u00af\7N\2\2\u00af\u00b0\7G\2\2\u00b0\u00b1\7N\2\2\u00b1")
        buf.write('"\3\2\2\2\u00b2\u00b3\7L\2\2\u00b3\u00b4\7q\2\2\u00b4')
        buf.write("\u00b5\7k\2\2\u00b5\u00b6\7p\2\2\u00b6\u00b7\7g\2\2\u00b7")
        buf.write("\u00b8\7t\2\2\u00b8$\3\2\2\2\u00b9\u00ba\7E\2\2\u00ba")
        buf.write("\u00bb\7q\2\2\u00bb\u00bc\7r\2\2\u00bc\u00bd\7{\2\2\u00bd")
        buf.write("&\3\2\2\2\u00be\u00bf\7T\2\2\u00bf\u00c0\7g\2\2\u00c0")
        buf.write("\u00c1\7p\2\2\u00c1\u00c2\7c\2\2\u00c2\u00c3\7o\2\2\u00c3")
        buf.write("\u00c4\7g\2\2\u00c4(\3\2\2\2\u00c5\u00c6\7V\2\2\u00c6")
        buf.write("\u00c7\7g\2\2\u00c7\u00c8\7u\2\2\u00c8\u00c9\7v\2\2\u00c9")
        buf.write("\u00ca\7U\2\2\u00ca\u00cb\7r\2\2\u00cb\u00cc\7g\2\2\u00cc")
        buf.write("\u00cd\7e\2\2\u00cd\u00ce\7V\2\2\u00ce\u00cf\7q\2\2\u00cf")
        buf.write("\u00d0\7U\2\2\u00d0\u00d1\7r\2\2\u00d1\u00d2\7g\2\2\u00d2")
        buf.write("\u00d3\7e\2\2\u00d3\u00d4\7*\2\2\u00d4\u00d5\7+\2\2\u00d5")
        buf.write("*\3\2\2\2\u00d6\u00d7\7U\2\2\u00d7\u00d8\7r\2\2\u00d8")
        buf.write("\u00d9\7g\2\2\u00d9\u00da\7e\2\2\u00da\u00db\7V\2\2\u00db")
        buf.write("\u00dc\7q\2\2\u00dc\u00dd\7V\2\2\u00dd\u00de\7g\2\2\u00de")
        buf.write("\u00df\7u\2\2\u00df\u00e0\7v\2\2\u00e0\u00e1\7U\2\2\u00e1")
        buf.write("\u00e2\7r\2\2\u00e2\u00e3\7g\2\2\u00e3\u00e4\7e\2\2\u00e4")
        buf.write("\u00e5\7*\2\2\u00e5\u00e6\7+\2\2\u00e6,\3\2\2\2\u00e7")
        buf.write("\u00e8\7K\2\2\u00e8\u00e9\7f\2\2\u00e9\u00ea\7g\2\2\u00ea")
        buf.write("\u00eb\7p\2\2\u00eb\u00ec\7v\2\2\u00ec\u00ed\7k\2\2\u00ed")
        buf.write("\u00ee\7v\2\2\u00ee\u00ef\7{\2\2\u00ef.\3\2\2\2\u00f0")
        buf.write("\u00f1\7C\2\2\u00f1\u00f2\7t\2\2\u00f2\u00f3\7v\2\2\u00f3")
        buf.write("\u00f4\7k\2\2\u00f4\u00f5\7h\2\2\u00f5\u00f6\7c\2\2\u00f6")
        buf.write("\u00f7\7e\2\2\u00f7\u00f8\7v\2\2\u00f8\u00f9\7H\2\2\u00f9")
        buf.write("\u00fa\7c\2\2\u00fa\u00fb\7e\2\2\u00fb\u00fc\7v\2\2\u00fc")
        buf.write("\u00fd\7q\2\2\u00fd\u00fe\7t\2\2\u00fe\u00ff\7{\2\2\u00ff")
        buf.write("\u0100\7\60\2\2\u0100\u0101\7e\2\2\u0101\u0102\7t\2\2")
        buf.write("\u0102\u0103\7g\2\2\u0103\u0104\7c\2\2\u0104\u0105\7v")
        buf.write("\2\2\u0105\u0106\7g\2\2\u0106\u0107\7*\2\2\u0107\60\3")
        buf.write("\2\2\2\u0108\u0109\7\60\2\2\u0109\62\3\2\2\2\u010a\u010b")
        buf.write("\7P\2\2\u010b\u010c\7Q\2\2\u010c\u010d\7V\2\2\u010d\64")
        buf.write("\3\2\2\2\u010e\u010f\7K\2\2\u010f\u0110\7P\2\2\u0110\u0111")
        buf.write("\7U\2\2\u0111\u0112\7V\2\2\u0112\u0113\7C\2\2\u0113\u0114")
        buf.write("\7P\2\2\u0114\u0115\7E\2\2\u0115\u0116\7G\2\2\u0116\u0117")
        buf.write("\7Q\2\2\u0117\u0118\7H\2\2\u0118\66\3\2\2\2\u0119\u011a")
        buf.write("\7G\2\2\u011a\u011b\7N\2\2\u011b\u011c\7G\2\2\u011c\u011d")
        buf.write("\7O\2\2\u011d\u011e\7G\2\2\u011e\u011f\7P\2\2\u011f\u0120")
        buf.write("\7V\2\2\u0120\u0121\7Q\2\2\u0121\u0122\7H\2\2\u01228\3")
        buf.write("\2\2\2\u0123\u0124\7V\2\2\u0124\u0125\7Q\2\2\u0125\u0126")
        buf.write("\7F\2\2\u0126\u0127\7Q\2\2\u0127\u0128\7/\2\2\u0128\u0129")
        buf.write("\7/\2\2\u0129\u012a\7/\2\2\u012a:\3\2\2\2\u012b\u012c")
        buf.write("\7)\2\2\u012c<\3\2\2\2\u012d\u012e\7H\2\2\u012e\u012f")
        buf.write("\7C\2\2\u012f\u0130\7N\2\2\u0130\u0131\7U\2\2\u0131\u0132")
        buf.write("\7G\2\2\u0132\u0133\3\2\2\2\u0133\u0144\b\37\2\2\u0134")
        buf.write("\u0135\7V\2\2\u0135\u0136\7T\2\2\u0136\u0137\7W\2\2\u0137")
        buf.write("\u0138\7G\2\2\u0138\u0139\3\2\2\2\u0139\u0144\b\37\3\2")
        buf.write("\u013a\u013b\7W\2\2\u013b\u013c\7P\2\2\u013c\u013d\7M")
        buf.write("\2\2\u013d\u013e\7P\2\2\u013e\u013f\7Q\2\2\u013f\u0140")
        buf.write("\7Y\2\2\u0140\u0141\7P\2\2\u0141\u0142\3\2\2\2\u0142\u0144")
        buf.write("\b\37\4\2\u0143\u012d\3\2\2\2\u0143\u0134\3\2\2\2\u0143")
        buf.write("\u013a\3\2\2\2\u0144>\3\2\2\2\u0145\u0146\7C\2\2\u0146")
        buf.write("\u0147\7P\2\2\u0147\u014f\7F\2\2\u0148\u0149\7Q\2\2\u0149")
        buf.write("\u014f\7T\2\2\u014a\u014b\7?\2\2\u014b\u014f\7?\2\2\u014c")
        buf.write("\u014d\7#\2\2\u014d\u014f\7?\2\2\u014e\u0145\3\2\2\2\u014e")
        buf.write("\u0148\3\2\2\2\u014e\u014a\3\2\2\2\u014e\u014c\3\2\2\2")
        buf.write("\u014f@\3\2\2\2\u0150\u0156\5I%\2\u0151\u0155\5G$\2\u0152")
        buf.write("\u0155\5M'\2\u0153\u0155\t\2\2\2\u0154\u0151\3\2\2\2")
        buf.write("\u0154\u0152\3\2\2\2\u0154\u0153\3\2\2\2\u0155\u0158\3")
        buf.write("\2\2\2\u0156\u0154\3\2\2\2\u0156\u0157\3\2\2\2\u0157B")
        buf.write("\3\2\2\2\u0158\u0156\3\2\2\2\u0159\u015d\7$\2\2\u015a")
        buf.write("\u015c\n\3\2\2\u015b\u015a\3\2\2\2\u015c\u015f\3\2\2\2")
        buf.write("\u015d\u015b\3\2\2\2\u015d\u015e\3\2\2\2\u015e\u0160\3")
        buf.write("\2\2\2\u015f\u015d\3\2\2\2\u0160\u0161\7$\2\2\u0161D\3")
        buf.write("\2\2\2\u0162\u0167\5K&\2\u0163\u0166\5G$\2\u0164\u0166")
        buf.write("\5M'\2\u0165\u0163\3\2\2\2\u0165\u0164\3\2\2\2\u0166")
        buf.write("\u0169\3\2\2\2\u0167\u0165\3\2\2\2\u0167\u0168\3\2\2\2")
        buf.write("\u0168F\3\2\2\2\u0169\u0167\3\2\2\2\u016a\u016d\5I%\2")
        buf.write("\u016b\u016d\5K&\2\u016c\u016a\3\2\2\2\u016c\u016b\3\2")
        buf.write("\2\2\u016dH\3\2\2\2\u016e\u016f\t\4\2\2\u016fJ\3\2\2\2")
        buf.write("\u0170\u0171\t\5\2\2\u0171L\3\2\2\2\u0172\u0173\t\6\2")
        buf.write("\2\u0173N\3\2\2\2\u0174\u0176\7\17\2\2\u0175\u0174\3\2")
        buf.write("\2\2\u0175\u0176\3\2\2\2\u0176\u0177\3\2\2\2\u0177\u0178")
        buf.write("\7\f\2\2\u0178\u0179\3\2\2\2\u0179\u017a\b(\5\2\u017a")
        buf.write("P\3\2\2\2\u017b\u017d\t\7\2\2\u017c\u017b\3\2\2\2\u017d")
        buf.write("\u017e\3\2\2\2\u017e\u017c\3\2\2\2\u017e\u017f\3\2\2\2")
        buf.write("\u017f\u0180\3\2\2\2\u0180\u0181\b)\5\2\u0181R\3\2\2\2")
        buf.write("\u0182\u0183\7=\2\2\u0183T\3\2\2\2\u0184\u0185\7\61\2")
        buf.write("\2\u0185\u0186\7\61\2\2\u0186\u018a\3\2\2\2\u0187\u0189")
        buf.write("\n\b\2\2\u0188\u0187\3\2\2\2\u0189\u018c\3\2\2\2\u018a")
        buf.write("\u0188\3\2\2\2\u018a\u018b\3\2\2\2\u018b\u018d\3\2\2\2")
        buf.write("\u018c\u018a\3\2\2\2\u018d\u018e\b+\5\2\u018eV\3\2\2\2")
        buf.write("\16\2\u0143\u014e\u0154\u0156\u015d\u0165\u0167\u016c")
        buf.write("\u0175\u017e\u018a\6\3\37\2\3\37\3\3\37\4\b\2\2")
        return buf.getvalue()


class CoVeriLangLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [DFA(ds, i) for i, ds in enumerate(atn.decisionToState)]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    T__22 = 23
    T__23 = 24
    T__24 = 25
    T__25 = 26
    T__26 = 27
    T__27 = 28
    T__28 = 29
    VERDICT = 30
    BIN_OP = 31
    ID = 32
    STRING = 33
    TYPE_NAME = 34
    LETTER = 35
    LOWER_CASE = 36
    UPPER_CASE = 37
    DIGIT = 38
    NEWLINE = 39
    WS = 40
    DELIMITER = 41
    COMMENT = 42

    channelNames = [u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN"]

    modeNames = ["DEFAULT_MODE"]

    literalNames = [
        "<INVALID>",
        "'fun'",
        "'('",
        "')'",
        "'{'",
        "'}'",
        "','",
        "'='",
        "'print('",
        "'execute('",
        "'return'",
        "':'",
        "'ActorFactory.create('",
        "'SEQUENCE'",
        "'ITE'",
        "'REPEAT'",
        "'PARALLEL'",
        "'Joiner'",
        "'Copy'",
        "'Rename'",
        "'TestSpecToSpec()'",
        "'SpecToTestSpec()'",
        "'Identity'",
        "'ArtifactFactory.create('",
        "'.'",
        "'NOT'",
        "'INSTANCEOF'",
        "'ELEMENTOF'",
        "'TODO---'",
        "'''",
        "';'",
    ]

    symbolicNames = [
        "<INVALID>",
        "VERDICT",
        "BIN_OP",
        "ID",
        "STRING",
        "TYPE_NAME",
        "LETTER",
        "LOWER_CASE",
        "UPPER_CASE",
        "DIGIT",
        "NEWLINE",
        "WS",
        "DELIMITER",
        "COMMENT",
    ]

    ruleNames = [
        "T__0",
        "T__1",
        "T__2",
        "T__3",
        "T__4",
        "T__5",
        "T__6",
        "T__7",
        "T__8",
        "T__9",
        "T__10",
        "T__11",
        "T__12",
        "T__13",
        "T__14",
        "T__15",
        "T__16",
        "T__17",
        "T__18",
        "T__19",
        "T__20",
        "T__21",
        "T__22",
        "T__23",
        "T__24",
        "T__25",
        "T__26",
        "T__27",
        "T__28",
        "VERDICT",
        "BIN_OP",
        "ID",
        "STRING",
        "TYPE_NAME",
        "LETTER",
        "LOWER_CASE",
        "UPPER_CASE",
        "DIGIT",
        "NEWLINE",
        "WS",
        "DELIMITER",
        "COMMENT",
    ]

    grammarFileName = "CoVeriLang.g4"

    def __init__(self, input=None, output: TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(
            self, self.atn, self.decisionsToDFA, PredictionContextCache()
        )
        self._actions = None
        self._predicates = None

    def action(self, localctx: RuleContext, ruleIndex: int, actionIndex: int):
        if self._actions is None:
            actions = dict()
            actions[29] = self.VERDICT_action
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def VERDICT_action(self, localctx: RuleContext, actionIndex: int):
        if actionIndex == 0:
            self.text = "RESULT_CLASS_FALSE"

        if actionIndex == 1:
            self.text = "RESULT_CLASS_TRUE"

        if actionIndex == 2:
            self.text = "RESULT_CLASS_OTHER"
