# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Generated from CoVeriLang.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys

if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3,")
        buf.write("\u0121\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\3\2\7\2\60\n")
        buf.write("\2\f\2\16\2\63\13\2\3\2\3\2\3\3\3\3\3\3\3\3\5\3;\n\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\7\4E\n\4\f\4\16\4H\13\4")
        buf.write("\3\5\3\5\3\5\7\5M\n\5\f\5\16\5P\13\5\3\6\3\6\3\6\3\6\5")
        buf.write("\6V\n\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\5\b`\n\b\3\b\3")
        buf.write("\b\3\t\3\t\3\t\3\t\3\t\5\ti\n\t\3\t\3\t\3\n\3\n\3\n\3")
        buf.write("\13\3\13\3\13\3\13\3\f\3\f\3\f\7\fw\n\f\f\f\16\fz\13\f")
        buf.write("\3\r\3\r\3\r\3\r\3\r\5\r\u0081\n\r\5\r\u0083\n\r\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\16\5\16\u008b\n\16\3\17\3\17\3")
        buf.write("\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u0097\n\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\5\17\u00a9\n\17\3\17\3\17\3")
        buf.write("\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u00c0\n")
        buf.write("\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\5\20\u00db\n\20\3\20\3\20\5\20\u00df")
        buf.write("\n\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\5\21\u00eb\n\21\3\22\3\22\3\23\3\23\3\24\3\24\3\24\3")
        buf.write("\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24")
        buf.write("\u0109\n\24\3\24\3\24\3\24\7\24\u010e\n\24\f\24\16\24")
        buf.write("\u0111\13\24\3\25\3\25\3\25\7\25\u0116\n\25\f\25\16\25")
        buf.write("\u0119\13\25\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\61\3")
        buf.write('&\30\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 "$&(*,\2')
        buf.write('\3\3\2"#\2\u0135\2\61\3\2\2\2\4\66\3\2\2\2\6F\3\2\2\2')
        buf.write("\bI\3\2\2\2\nU\3\2\2\2\fW\3\2\2\2\16[\3\2\2\2\20c\3\2")
        buf.write("\2\2\22l\3\2\2\2\24o\3\2\2\2\26s\3\2\2\2\30{\3\2\2\2\32")
        buf.write("\u008a\3\2\2\2\34\u00bf\3\2\2\2\36\u00de\3\2\2\2 \u00ea")
        buf.write('\3\2\2\2"\u00ec\3\2\2\2$\u00ee\3\2\2\2&\u0108\3\2\2\2')
        buf.write("(\u0112\3\2\2\2*\u011a\3\2\2\2,\u011c\3\2\2\2.\60\5\4")
        buf.write("\3\2/.\3\2\2\2\60\63\3\2\2\2\61\62\3\2\2\2\61/\3\2\2\2")
        buf.write("\62\64\3\2\2\2\63\61\3\2\2\2\64\65\5\6\4\2\65\3\3\2\2")
        buf.write('\2\66\67\7\3\2\2\678\7"\2\28:\7\4\2\29;\5\b\5\2:9\3\2')
        buf.write("\2\2:;\3\2\2\2;<\3\2\2\2<=\7\5\2\2=>\7\6\2\2>?\5\6\4\2")
        buf.write("?@\7\7\2\2@\5\3\2\2\2AB\5\n\6\2BC\7+\2\2CE\3\2\2\2DA\3")
        buf.write("\2\2\2EH\3\2\2\2FD\3\2\2\2FG\3\2\2\2G\7\3\2\2\2HF\3\2")
        buf.write('\2\2IN\7"\2\2JK\7\b\2\2KM\7"\2\2LJ\3\2\2\2MP\3\2\2\2')
        buf.write("NL\3\2\2\2NO\3\2\2\2O\t\3\2\2\2PN\3\2\2\2QV\5\f\7\2RV")
        buf.write("\5\20\t\2SV\5\22\n\2TV\5\16\b\2UQ\3\2\2\2UR\3\2\2\2US")
        buf.write('\3\2\2\2UT\3\2\2\2V\13\3\2\2\2WX\7"\2\2XY\7\t\2\2YZ\5')
        buf.write("\32\16\2Z\r\3\2\2\2[_\7\n\2\2\\`\5\34\17\2]`\7#\2\2^`")
        buf.write('\7"\2\2_\\\3\2\2\2_]\3\2\2\2_^\3\2\2\2`a\3\2\2\2ab\7')
        buf.write("\5\2\2b\17\3\2\2\2cd\7\13\2\2de\5\34\17\2eh\7\b\2\2fi")
        buf.write('\7"\2\2gi\5\24\13\2hf\3\2\2\2hg\3\2\2\2ij\3\2\2\2jk\7')
        buf.write('\5\2\2k\21\3\2\2\2lm\7\f\2\2mn\7"\2\2n\23\3\2\2\2op\7')
        buf.write("\6\2\2pq\5\26\f\2qr\7\7\2\2r\25\3\2\2\2sx\5\30\r\2tu\7")
        buf.write("\b\2\2uw\5\30\r\2vt\3\2\2\2wz\3\2\2\2xv\3\2\2\2xy\3\2")
        buf.write("\2\2y\27\3\2\2\2zx\3\2\2\2{\u0082\5,\27\2|\u0080\7\r\2")
        buf.write('\2}\u0081\5 \21\2~\u0081\5"\22\2\177\u0081\5,\27\2\u0080')
        buf.write("}\3\2\2\2\u0080~\3\2\2\2\u0080\177\3\2\2\2\u0081\u0083")
        buf.write("\3\2\2\2\u0082|\3\2\2\2\u0082\u0083\3\2\2\2\u0083\31\3")
        buf.write("\2\2\2\u0084\u008b\5&\24\2\u0085\u008b\5 \21\2\u0086\u008b")
        buf.write("\5\24\13\2\u0087\u008b\5\34\17\2\u0088\u008b\7#\2\2\u0089")
        buf.write("\u008b\5\20\t\2\u008a\u0084\3\2\2\2\u008a\u0085\3\2\2")
        buf.write("\2\u008a\u0086\3\2\2\2\u008a\u0087\3\2\2\2\u008a\u0088")
        buf.write("\3\2\2\2\u008a\u0089\3\2\2\2\u008b\33\3\2\2\2\u008c\u008d")
        buf.write("\7\16\2\2\u008d\u008e\5$\23\2\u008e\u008f\7\b\2\2\u008f")
        buf.write("\u0090\t\2\2\2\u0090\u0091\3\2\2\2\u0091\u0092\7\5\2\2")
        buf.write('\u0092\u00c0\3\2\2\2\u0093\u0094\7"\2\2\u0094\u0096\7')
        buf.write("\4\2\2\u0095\u0097\5\b\5\2\u0096\u0095\3\2\2\2\u0096\u0097")
        buf.write("\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u00c0\7\5\2\2\u0099")
        buf.write("\u00c0\5\36\20\2\u009a\u009b\7\17\2\2\u009b\u009c\7\4")
        buf.write("\2\2\u009c\u009d\5\34\17\2\u009d\u009e\7\b\2\2\u009e\u009f")
        buf.write("\5\34\17\2\u009f\u00a0\7\5\2\2\u00a0\u00c0\3\2\2\2\u00a1")
        buf.write("\u00a2\7\20\2\2\u00a2\u00a3\7\4\2\2\u00a3\u00a4\5&\24")
        buf.write("\2\u00a4\u00a5\7\b\2\2\u00a5\u00a8\5\34\17\2\u00a6\u00a7")
        buf.write("\7\b\2\2\u00a7\u00a9\5\34\17\2\u00a8\u00a6\3\2\2\2\u00a8")
        buf.write("\u00a9\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab\7\5\2\2")
        buf.write("\u00ab\u00c0\3\2\2\2\u00ac\u00ad\7\21\2\2\u00ad\u00ae")
        buf.write("\7\4\2\2\u00ae\u00af\5,\27\2\u00af\u00b0\7\b\2\2\u00b0")
        buf.write("\u00b1\5\34\17\2\u00b1\u00b2\7\5\2\2\u00b2\u00c0\3\2\2")
        buf.write("\2\u00b3\u00b4\7\22\2\2\u00b4\u00b5\7\4\2\2\u00b5\u00b6")
        buf.write("\5\34\17\2\u00b6\u00b7\7\b\2\2\u00b7\u00b8\5\34\17\2\u00b8")
        buf.write('\u00b9\7\5\2\2\u00b9\u00c0\3\2\2\2\u00ba\u00c0\7"\2\2')
        buf.write("\u00bb\u00bc\7\4\2\2\u00bc\u00bd\5\34\17\2\u00bd\u00be")
        buf.write("\7\5\2\2\u00be\u00c0\3\2\2\2\u00bf\u008c\3\2\2\2\u00bf")
        buf.write("\u0093\3\2\2\2\u00bf\u0099\3\2\2\2\u00bf\u009a\3\2\2\2")
        buf.write("\u00bf\u00a1\3\2\2\2\u00bf\u00ac\3\2\2\2\u00bf\u00b3\3")
        buf.write("\2\2\2\u00bf\u00ba\3\2\2\2\u00bf\u00bb\3\2\2\2\u00c0\35")
        buf.write("\3\2\2\2\u00c1\u00c2\7\23\2\2\u00c2\u00c3\7\4\2\2\u00c3")
        buf.write('\u00c4\5"\22\2\u00c4\u00c5\7\b\2\2\u00c5\u00c6\5\24\13')
        buf.write("\2\u00c6\u00c7\7\b\2\2\u00c7\u00c8\5,\27\2\u00c8\u00c9")
        buf.write("\7\5\2\2\u00c9\u00df\3\2\2\2\u00ca\u00cb\7\24\2\2\u00cb")
        buf.write("\u00cc\7\4\2\2\u00cc\u00cd\5\24\13\2\u00cd\u00ce\7\5\2")
        buf.write("\2\u00ce\u00df\3\2\2\2\u00cf\u00d0\7\25\2\2\u00d0\u00d1")
        buf.write("\7\4\2\2\u00d1\u00d2\5\24\13\2\u00d2\u00d3\7\5\2\2\u00d3")
        buf.write("\u00df\3\2\2\2\u00d4\u00df\7\26\2\2\u00d5\u00df\7\27\2")
        buf.write("\2\u00d6\u00d7\7\30\2\2\u00d7\u00da\7\4\2\2\u00d8\u00db")
        buf.write("\5\34\17\2\u00d9\u00db\5\24\13\2\u00da\u00d8\3\2\2\2\u00da")
        buf.write("\u00d9\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00dd\7\5\2\2")
        buf.write("\u00dd\u00df\3\2\2\2\u00de\u00c1\3\2\2\2\u00de\u00ca\3")
        buf.write("\2\2\2\u00de\u00cf\3\2\2\2\u00de\u00d4\3\2\2\2\u00de\u00d5")
        buf.write("\3\2\2\2\u00de\u00d6\3\2\2\2\u00df\37\3\2\2\2\u00e0\u00e1")
        buf.write('\7\31\2\2\u00e1\u00e2\5"\22\2\u00e2\u00e3\7\b\2\2\u00e3')
        buf.write("\u00e4\t\2\2\2\u00e4\u00e5\7\5\2\2\u00e5\u00eb\3\2\2\2")
        buf.write('\u00e6\u00eb\7"\2\2\u00e7\u00e8\7"\2\2\u00e8\u00e9\7')
        buf.write('\32\2\2\u00e9\u00eb\7"\2\2\u00ea\u00e0\3\2\2\2\u00ea')
        buf.write("\u00e6\3\2\2\2\u00ea\u00e7\3\2\2\2\u00eb!\3\2\2\2\u00ec")
        buf.write("\u00ed\7$\2\2\u00ed#\3\2\2\2\u00ee\u00ef\7$\2\2\u00ef")
        buf.write("%\3\2\2\2\u00f0\u00f1\b\24\1\2\u00f1\u00f2\7\33\2\2\u00f2")
        buf.write("\u0109\5&\24\7\u00f3\u00f4\7\34\2\2\u00f4\u00f5\7\4\2")
        buf.write('\2\u00f5\u00f6\7"\2\2\u00f6\u00f7\7\b\2\2\u00f7\u00f8')
        buf.write('\5"\22\2\u00f8\u00f9\7\5\2\2\u00f9\u0109\3\2\2\2\u00fa')
        buf.write('\u00fb\7\35\2\2\u00fb\u00fc\7\4\2\2\u00fc\u00fd\7"\2')
        buf.write("\2\u00fd\u00fe\7\b\2\2\u00fe\u00ff\7\6\2\2\u00ff\u0100")
        buf.write("\5(\25\2\u0100\u0101\7\7\2\2\u0101\u0102\7\5\2\2\u0102")
        buf.write('\u0109\3\2\2\2\u0103\u0109\7"\2\2\u0104\u0105\7\4\2\2')
        buf.write("\u0105\u0106\5&\24\2\u0106\u0107\7\5\2\2\u0107\u0109\3")
        buf.write("\2\2\2\u0108\u00f0\3\2\2\2\u0108\u00f3\3\2\2\2\u0108\u00fa")
        buf.write("\3\2\2\2\u0108\u0103\3\2\2\2\u0108\u0104\3\2\2\2\u0109")
        buf.write("\u010f\3\2\2\2\u010a\u010b\f\b\2\2\u010b\u010c\7!\2\2")
        buf.write("\u010c\u010e\5&\24\t\u010d\u010a\3\2\2\2\u010e\u0111\3")
        buf.write("\2\2\2\u010f\u010d\3\2\2\2\u010f\u0110\3\2\2\2\u0110'")
        buf.write("\3\2\2\2\u0111\u010f\3\2\2\2\u0112\u0117\7 \2\2\u0113")
        buf.write("\u0114\7\b\2\2\u0114\u0116\7 \2\2\u0115\u0113\3\2\2\2")
        buf.write("\u0116\u0119\3\2\2\2\u0117\u0115\3\2\2\2\u0117\u0118\3")
        buf.write("\2\2\2\u0118)\3\2\2\2\u0119\u0117\3\2\2\2\u011a\u011b")
        buf.write("\7\36\2\2\u011b+\3\2\2\2\u011c\u011d\7\37\2\2\u011d\u011e")
        buf.write('\7"\2\2\u011e\u011f\7\37\2\2\u011f-\3\2\2\2\26\61:FN')
        buf.write("U_hx\u0080\u0082\u008a\u0096\u00a8\u00bf\u00da\u00de\u00ea")
        buf.write("\u0108\u010f\u0117")
        return buf.getvalue()


class CoVeriLangParser(Parser):

    grammarFileName = "CoVeriLang.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [DFA(ds, i) for i, ds in enumerate(atn.decisionToState)]

    sharedContextCache = PredictionContextCache()

    literalNames = [
        "<INVALID>",
        "'fun'",
        "'('",
        "')'",
        "'{'",
        "'}'",
        "','",
        "'='",
        "'print('",
        "'execute('",
        "'return'",
        "':'",
        "'ActorFactory.create('",
        "'SEQUENCE'",
        "'ITE'",
        "'REPEAT'",
        "'PARALLEL'",
        "'Joiner'",
        "'Copy'",
        "'Rename'",
        "'TestSpecToSpec()'",
        "'SpecToTestSpec()'",
        "'Identity'",
        "'ArtifactFactory.create('",
        "'.'",
        "'NOT'",
        "'INSTANCEOF'",
        "'ELEMENTOF'",
        "'TODO---'",
        "'''",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "';'",
    ]

    symbolicNames = [
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "VERDICT",
        "BIN_OP",
        "ID",
        "STRING",
        "TYPE_NAME",
        "LETTER",
        "LOWER_CASE",
        "UPPER_CASE",
        "DIGIT",
        "NEWLINE",
        "WS",
        "DELIMITER",
        "COMMENT",
    ]

    RULE_program = 0
    RULE_fun_decl = 1
    RULE_stmt_block = 2
    RULE_id_list = 3
    RULE_stmt = 4
    RULE_spec_stmt = 5
    RULE_print_stmt = 6
    RULE_exec_stmt = 7
    RULE_return_stmt = 8
    RULE_arg_map = 9
    RULE_map_item_list = 10
    RULE_map_item = 11
    RULE_assignable = 12
    RULE_actor = 13
    RULE_utility_actor = 14
    RULE_artifact = 15
    RULE_artifact_type = 16
    RULE_actor_type = 17
    RULE_exp = 18
    RULE_verdict_list = 19
    RULE_tc_exp = 20
    RULE_quoted_ID = 21

    ruleNames = [
        "program",
        "fun_decl",
        "stmt_block",
        "id_list",
        "stmt",
        "spec_stmt",
        "print_stmt",
        "exec_stmt",
        "return_stmt",
        "arg_map",
        "map_item_list",
        "map_item",
        "assignable",
        "actor",
        "utility_actor",
        "artifact",
        "artifact_type",
        "actor_type",
        "exp",
        "verdict_list",
        "tc_exp",
        "quoted_ID",
    ]

    EOF = Token.EOF
    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    T__22 = 23
    T__23 = 24
    T__24 = 25
    T__25 = 26
    T__26 = 27
    T__27 = 28
    T__28 = 29
    VERDICT = 30
    BIN_OP = 31
    ID = 32
    STRING = 33
    TYPE_NAME = 34
    LETTER = 35
    LOWER_CASE = 36
    UPPER_CASE = 37
    DIGIT = 38
    NEWLINE = 39
    WS = 40
    DELIMITER = 41
    COMMENT = 42

    def __init__(self, input: TokenStream, output: TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(
            self, self.atn, self.decisionsToDFA, self.sharedContextCache
        )
        self._predicates = None

    class ProgramContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stmt_block(self):
            return self.getTypedRuleContext(CoVeriLangParser.Stmt_blockContext, 0)

        def fun_decl(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.Fun_declContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.Fun_declContext, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_program

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterProgram"):
                listener.enterProgram(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitProgram"):
                listener.exitProgram(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitProgram"):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)

    def program(self):

        localctx = CoVeriLangParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 47
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input, 0, self._ctx)
            while _alt != 1 and _alt != ATN.INVALID_ALT_NUMBER:
                if _alt == 1 + 1:
                    self.state = 44
                    self.fun_decl()
                self.state = 49
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input, 0, self._ctx)

            self.state = 50
            self.stmt_block()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Fun_declContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def stmt_block(self):
            return self.getTypedRuleContext(CoVeriLangParser.Stmt_blockContext, 0)

        def id_list(self):
            return self.getTypedRuleContext(CoVeriLangParser.Id_listContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_fun_decl

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterFun_decl"):
                listener.enterFun_decl(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitFun_decl"):
                listener.exitFun_decl(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitFun_decl"):
                return visitor.visitFun_decl(self)
            else:
                return visitor.visitChildren(self)

    def fun_decl(self):

        localctx = CoVeriLangParser.Fun_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_fun_decl)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self.match(CoVeriLangParser.T__0)
            self.state = 53
            self.match(CoVeriLangParser.ID)
            self.state = 54
            self.match(CoVeriLangParser.T__1)
            self.state = 56
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la == CoVeriLangParser.ID:
                self.state = 55
                self.id_list()

            self.state = 58
            self.match(CoVeriLangParser.T__2)
            self.state = 59
            self.match(CoVeriLangParser.T__3)
            self.state = 60
            self.stmt_block()
            self.state = 61
            self.match(CoVeriLangParser.T__4)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Stmt_blockContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stmt(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.StmtContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.StmtContext, i)

        def DELIMITER(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.DELIMITER)
            else:
                return self.getToken(CoVeriLangParser.DELIMITER, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_stmt_block

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterStmt_block"):
                listener.enterStmt_block(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitStmt_block"):
                listener.exitStmt_block(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitStmt_block"):
                return visitor.visitStmt_block(self)
            else:
                return visitor.visitChildren(self)

    def stmt_block(self):

        localctx = CoVeriLangParser.Stmt_blockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_stmt_block)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 68
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((_la) & ~0x3F) == 0 and (
                (1 << _la)
                & (
                    (1 << CoVeriLangParser.T__7)
                    | (1 << CoVeriLangParser.T__8)
                    | (1 << CoVeriLangParser.T__9)
                    | (1 << CoVeriLangParser.ID)
                )
            ) != 0:
                self.state = 63
                self.stmt()
                self.state = 64
                self.match(CoVeriLangParser.DELIMITER)
                self.state = 70
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Id_listContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.ID)
            else:
                return self.getToken(CoVeriLangParser.ID, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_id_list

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterId_list"):
                listener.enterId_list(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitId_list"):
                listener.exitId_list(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitId_list"):
                return visitor.visitId_list(self)
            else:
                return visitor.visitChildren(self)

    def id_list(self):

        localctx = CoVeriLangParser.Id_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_id_list)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 71
            self.match(CoVeriLangParser.ID)
            self.state = 76
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == CoVeriLangParser.T__5:
                self.state = 72
                self.match(CoVeriLangParser.T__5)
                self.state = 73
                self.match(CoVeriLangParser.ID)
                self.state = 78
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def spec_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Spec_stmtContext, 0)

        def exec_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Exec_stmtContext, 0)

        def return_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Return_stmtContext, 0)

        def print_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Print_stmtContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_stmt

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterStmt"):
                listener.enterStmt(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitStmt"):
                listener.exitStmt(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitStmt"):
                return visitor.visitStmt(self)
            else:
                return visitor.visitChildren(self)

    def stmt(self):

        localctx = CoVeriLangParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_stmt)
        try:
            self.state = 83
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 79
                self.spec_stmt()
                pass
            elif token in [CoVeriLangParser.T__8]:
                self.enterOuterAlt(localctx, 2)
                self.state = 80
                self.exec_stmt()
                pass
            elif token in [CoVeriLangParser.T__9]:
                self.enterOuterAlt(localctx, 3)
                self.state = 81
                self.return_stmt()
                pass
            elif token in [CoVeriLangParser.T__7]:
                self.enterOuterAlt(localctx, 4)
                self.state = 82
                self.print_stmt()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Spec_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def assignable(self):
            return self.getTypedRuleContext(CoVeriLangParser.AssignableContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_spec_stmt

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSpec_stmt"):
                listener.enterSpec_stmt(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSpec_stmt"):
                listener.exitSpec_stmt(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitSpec_stmt"):
                return visitor.visitSpec_stmt(self)
            else:
                return visitor.visitChildren(self)

    def spec_stmt(self):

        localctx = CoVeriLangParser.Spec_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_spec_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            self.match(CoVeriLangParser.ID)
            self.state = 86
            self.match(CoVeriLangParser.T__6)
            self.state = 87
            self.assignable()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Print_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_print_stmt

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class PrintActorContext(Print_stmtContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Print_stmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def STRING(self):
            return self.getToken(CoVeriLangParser.STRING, 0)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterPrintActor"):
                listener.enterPrintActor(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitPrintActor"):
                listener.exitPrintActor(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitPrintActor"):
                return visitor.visitPrintActor(self)
            else:
                return visitor.visitChildren(self)

    def print_stmt(self):

        localctx = CoVeriLangParser.Print_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_print_stmt)
        try:
            localctx = CoVeriLangParser.PrintActorContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 89
            self.match(CoVeriLangParser.T__7)
            self.state = 93
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input, 5, self._ctx)
            if la_ == 1:
                self.state = 90
                self.actor()
                pass

            elif la_ == 2:
                self.state = 91
                self.match(CoVeriLangParser.STRING)
                pass

            elif la_ == 3:
                self.state = 92
                self.match(CoVeriLangParser.ID)
                pass

            self.state = 95
            self.match(CoVeriLangParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Exec_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_exec_stmt

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class ExecuteActorContext(Exec_stmtContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Exec_stmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterExecuteActor"):
                listener.enterExecuteActor(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitExecuteActor"):
                listener.exitExecuteActor(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitExecuteActor"):
                return visitor.visitExecuteActor(self)
            else:
                return visitor.visitChildren(self)

    def exec_stmt(self):

        localctx = CoVeriLangParser.Exec_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_exec_stmt)
        try:
            localctx = CoVeriLangParser.ExecuteActorContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 97
            self.match(CoVeriLangParser.T__8)
            self.state = 98
            self.actor()
            self.state = 99
            self.match(CoVeriLangParser.T__5)
            self.state = 102
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.ID]:
                self.state = 100
                self.match(CoVeriLangParser.ID)
                pass
            elif token in [CoVeriLangParser.T__3]:
                self.state = 101
                self.arg_map()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 104
            self.match(CoVeriLangParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Return_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_return_stmt

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterReturn_stmt"):
                listener.enterReturn_stmt(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitReturn_stmt"):
                listener.exitReturn_stmt(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitReturn_stmt"):
                return visitor.visitReturn_stmt(self)
            else:
                return visitor.visitChildren(self)

    def return_stmt(self):

        localctx = CoVeriLangParser.Return_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_return_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 106
            self.match(CoVeriLangParser.T__9)
            self.state = 107
            self.match(CoVeriLangParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arg_mapContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def map_item_list(self):
            return self.getTypedRuleContext(CoVeriLangParser.Map_item_listContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_arg_map

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterArg_map"):
                listener.enterArg_map(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitArg_map"):
                listener.exitArg_map(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitArg_map"):
                return visitor.visitArg_map(self)
            else:
                return visitor.visitChildren(self)

    def arg_map(self):

        localctx = CoVeriLangParser.Arg_mapContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_arg_map)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 109
            self.match(CoVeriLangParser.T__3)
            self.state = 110
            self.map_item_list()
            self.state = 111
            self.match(CoVeriLangParser.T__4)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Map_item_listContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def map_item(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.Map_itemContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.Map_itemContext, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_map_item_list

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterMap_item_list"):
                listener.enterMap_item_list(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitMap_item_list"):
                listener.exitMap_item_list(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitMap_item_list"):
                return visitor.visitMap_item_list(self)
            else:
                return visitor.visitChildren(self)

    def map_item_list(self):

        localctx = CoVeriLangParser.Map_item_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_map_item_list)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 113
            self.map_item()
            self.state = 118
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == CoVeriLangParser.T__5:
                self.state = 114
                self.match(CoVeriLangParser.T__5)
                self.state = 115
                self.map_item()
                self.state = 120
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Map_itemContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def quoted_ID(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.Quoted_IDContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, i)

        def artifact(self):
            return self.getTypedRuleContext(CoVeriLangParser.ArtifactContext, 0)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_map_item

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterMap_item"):
                listener.enterMap_item(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitMap_item"):
                listener.exitMap_item(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitMap_item"):
                return visitor.visitMap_item(self)
            else:
                return visitor.visitChildren(self)

    def map_item(self):

        localctx = CoVeriLangParser.Map_itemContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_map_item)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 121
            self.quoted_ID()
            self.state = 128
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la == CoVeriLangParser.T__10:
                self.state = 122
                self.match(CoVeriLangParser.T__10)
                self.state = 126
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [CoVeriLangParser.T__22, CoVeriLangParser.ID]:
                    self.state = 123
                    self.artifact()
                    pass
                elif token in [CoVeriLangParser.TYPE_NAME]:
                    self.state = 124
                    self.artifact_type()
                    pass
                elif token in [CoVeriLangParser.T__28]:
                    self.state = 125
                    self.quoted_ID()
                    pass
                else:
                    raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignableContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def artifact(self):
            return self.getTypedRuleContext(CoVeriLangParser.ArtifactContext, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def STRING(self):
            return self.getToken(CoVeriLangParser.STRING, 0)

        def exec_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Exec_stmtContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_assignable

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterAssignable"):
                listener.enterAssignable(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitAssignable"):
                listener.exitAssignable(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitAssignable"):
                return visitor.visitAssignable(self)
            else:
                return visitor.visitChildren(self)

    def assignable(self):

        localctx = CoVeriLangParser.AssignableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_assignable)
        try:
            self.state = 136
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input, 10, self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 130
                self.exp(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 131
                self.artifact()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 132
                self.arg_map()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 133
                self.actor()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 134
                self.match(CoVeriLangParser.STRING)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 135
                self.exec_stmt()
                pass

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ActorContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_actor

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class UtilityContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def utility_actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.Utility_actorContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterUtility"):
                listener.enterUtility(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitUtility"):
                listener.exitUtility(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitUtility"):
                return visitor.visitUtility(self)
            else:
                return visitor.visitChildren(self)

    class ParenthesisContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterParenthesis"):
                listener.enterParenthesis(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitParenthesis"):
                listener.exitParenthesis(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitParenthesis"):
                return visitor.visitParenthesis(self)
            else:
                return visitor.visitChildren(self)

    class FunCallContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def id_list(self):
            return self.getTypedRuleContext(CoVeriLangParser.Id_listContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterFunCall"):
                listener.enterFunCall(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitFunCall"):
                listener.exitFunCall(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitFunCall"):
                return visitor.visitFunCall(self)
            else:
                return visitor.visitChildren(self)

    class AtomicContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Actor_typeContext, 0)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def STRING(self):
            return self.getToken(CoVeriLangParser.STRING, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterAtomic"):
                listener.enterAtomic(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitAtomic"):
                listener.exitAtomic(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitAtomic"):
                return visitor.visitAtomic(self)
            else:
                return visitor.visitChildren(self)

    class IterativeContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def quoted_ID(self):
            return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, 0)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterIterative"):
                listener.enterIterative(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitIterative"):
                listener.exitIterative(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitIterative"):
                return visitor.visitIterative(self)
            else:
                return visitor.visitChildren(self)

    class SequenceContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ActorContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ActorContext, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSequence"):
                listener.enterSequence(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSequence"):
                listener.exitSequence(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitSequence"):
                return visitor.visitSequence(self)
            else:
                return visitor.visitChildren(self)

    class ITEContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def actor(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ActorContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ActorContext, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterITE"):
                listener.enterITE(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitITE"):
                listener.exitITE(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitITE"):
                return visitor.visitITE(self)
            else:
                return visitor.visitChildren(self)

    class ParallelContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ActorContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ActorContext, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterParallel"):
                listener.enterParallel(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitParallel"):
                listener.exitParallel(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitParallel"):
                return visitor.visitParallel(self)
            else:
                return visitor.visitChildren(self)

    class ActorAliasContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterActorAlias"):
                listener.enterActorAlias(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitActorAlias"):
                listener.exitActorAlias(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitActorAlias"):
                return visitor.visitActorAlias(self)
            else:
                return visitor.visitChildren(self)

    def actor(self):

        localctx = CoVeriLangParser.ActorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_actor)
        self._la = 0  # Token type
        try:
            self.state = 189
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input, 13, self._ctx)
            if la_ == 1:
                localctx = CoVeriLangParser.AtomicContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 138
                self.match(CoVeriLangParser.T__11)
                self.state = 139
                self.actor_type()

                self.state = 140
                self.match(CoVeriLangParser.T__5)
                self.state = 141
                _la = self._input.LA(1)
                if not (_la == CoVeriLangParser.ID or _la == CoVeriLangParser.STRING):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 143
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 2:
                localctx = CoVeriLangParser.FunCallContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 145
                self.match(CoVeriLangParser.ID)
                self.state = 146
                self.match(CoVeriLangParser.T__1)
                self.state = 148
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la == CoVeriLangParser.ID:
                    self.state = 147
                    self.id_list()

                self.state = 150
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 3:
                localctx = CoVeriLangParser.UtilityContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 151
                self.utility_actor()
                pass

            elif la_ == 4:
                localctx = CoVeriLangParser.SequenceContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 152
                self.match(CoVeriLangParser.T__12)
                self.state = 153
                self.match(CoVeriLangParser.T__1)
                self.state = 154
                self.actor()
                self.state = 155
                self.match(CoVeriLangParser.T__5)
                self.state = 156
                self.actor()
                self.state = 157
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 5:
                localctx = CoVeriLangParser.ITEContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 159
                self.match(CoVeriLangParser.T__13)
                self.state = 160
                self.match(CoVeriLangParser.T__1)
                self.state = 161
                self.exp(0)
                self.state = 162
                self.match(CoVeriLangParser.T__5)
                self.state = 163
                self.actor()
                self.state = 166
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la == CoVeriLangParser.T__5:
                    self.state = 164
                    self.match(CoVeriLangParser.T__5)
                    self.state = 165
                    self.actor()

                self.state = 168
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 6:
                localctx = CoVeriLangParser.IterativeContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 170
                self.match(CoVeriLangParser.T__14)
                self.state = 171
                self.match(CoVeriLangParser.T__1)
                self.state = 172
                self.quoted_ID()
                self.state = 173
                self.match(CoVeriLangParser.T__5)
                self.state = 174
                self.actor()
                self.state = 175
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 7:
                localctx = CoVeriLangParser.ParallelContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 177
                self.match(CoVeriLangParser.T__15)
                self.state = 178
                self.match(CoVeriLangParser.T__1)
                self.state = 179
                self.actor()
                self.state = 180
                self.match(CoVeriLangParser.T__5)
                self.state = 181
                self.actor()
                self.state = 182
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 8:
                localctx = CoVeriLangParser.ActorAliasContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 184
                self.match(CoVeriLangParser.ID)
                pass

            elif la_ == 9:
                localctx = CoVeriLangParser.ParenthesisContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 185
                self.match(CoVeriLangParser.T__1)
                self.state = 186
                self.actor()
                self.state = 187
                self.match(CoVeriLangParser.T__2)
                pass

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Utility_actorContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_utility_actor

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class JoinerContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def quoted_ID(self):
            return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterJoiner"):
                listener.enterJoiner(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitJoiner"):
                listener.exitJoiner(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitJoiner"):
                return visitor.visitJoiner(self)
            else:
                return visitor.visitChildren(self)

    class TestSpecToSpecContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterTestSpecToSpec"):
                listener.enterTestSpecToSpec(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitTestSpecToSpec"):
                listener.exitTestSpecToSpec(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitTestSpecToSpec"):
                return visitor.visitTestSpecToSpec(self)
            else:
                return visitor.visitChildren(self)

    class SpecToTestSpecContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSpecToTestSpec"):
                listener.enterSpecToTestSpec(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSpecToTestSpec"):
                listener.exitSpecToTestSpec(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitSpecToTestSpec"):
                return visitor.visitSpecToTestSpec(self)
            else:
                return visitor.visitChildren(self)

    class CopyContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterCopy"):
                listener.enterCopy(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitCopy"):
                listener.exitCopy(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitCopy"):
                return visitor.visitCopy(self)
            else:
                return visitor.visitChildren(self)

    class IdentityContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterIdentity"):
                listener.enterIdentity(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitIdentity"):
                listener.exitIdentity(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitIdentity"):
                return visitor.visitIdentity(self)
            else:
                return visitor.visitChildren(self)

    class RenameContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterRename"):
                listener.enterRename(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitRename"):
                listener.exitRename(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitRename"):
                return visitor.visitRename(self)
            else:
                return visitor.visitChildren(self)

    def utility_actor(self):

        localctx = CoVeriLangParser.Utility_actorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_utility_actor)
        try:
            self.state = 220
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.T__16]:
                localctx = CoVeriLangParser.JoinerContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 191
                self.match(CoVeriLangParser.T__16)
                self.state = 192
                self.match(CoVeriLangParser.T__1)
                self.state = 193
                self.artifact_type()
                self.state = 194
                self.match(CoVeriLangParser.T__5)
                self.state = 195
                self.arg_map()
                self.state = 196
                self.match(CoVeriLangParser.T__5)
                self.state = 197
                self.quoted_ID()
                self.state = 198
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__17]:
                localctx = CoVeriLangParser.CopyContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 200
                self.match(CoVeriLangParser.T__17)
                self.state = 201
                self.match(CoVeriLangParser.T__1)
                self.state = 202
                self.arg_map()
                self.state = 203
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__18]:
                localctx = CoVeriLangParser.RenameContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 205
                self.match(CoVeriLangParser.T__18)
                self.state = 206
                self.match(CoVeriLangParser.T__1)
                self.state = 207
                self.arg_map()
                self.state = 208
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__19]:
                localctx = CoVeriLangParser.TestSpecToSpecContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 210
                self.match(CoVeriLangParser.T__19)
                pass
            elif token in [CoVeriLangParser.T__20]:
                localctx = CoVeriLangParser.SpecToTestSpecContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 211
                self.match(CoVeriLangParser.T__20)
                pass
            elif token in [CoVeriLangParser.T__21]:
                localctx = CoVeriLangParser.IdentityContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 212
                self.match(CoVeriLangParser.T__21)
                self.state = 213
                self.match(CoVeriLangParser.T__1)
                self.state = 216
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [
                    CoVeriLangParser.T__1,
                    CoVeriLangParser.T__11,
                    CoVeriLangParser.T__12,
                    CoVeriLangParser.T__13,
                    CoVeriLangParser.T__14,
                    CoVeriLangParser.T__15,
                    CoVeriLangParser.T__16,
                    CoVeriLangParser.T__17,
                    CoVeriLangParser.T__18,
                    CoVeriLangParser.T__19,
                    CoVeriLangParser.T__20,
                    CoVeriLangParser.T__21,
                    CoVeriLangParser.ID,
                ]:
                    self.state = 214
                    self.actor()
                    pass
                elif token in [CoVeriLangParser.T__3]:
                    self.state = 215
                    self.arg_map()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 218
                self.match(CoVeriLangParser.T__2)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArtifactContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_artifact

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class ArtifactFromMapItemContext(ArtifactContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ArtifactContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.ID)
            else:
                return self.getToken(CoVeriLangParser.ID, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterArtifactFromMapItem"):
                listener.enterArtifactFromMapItem(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitArtifactFromMapItem"):
                listener.exitArtifactFromMapItem(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitArtifactFromMapItem"):
                return visitor.visitArtifactFromMapItem(self)
            else:
                return visitor.visitChildren(self)

    class ArtifactAliasContext(ArtifactContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ArtifactContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterArtifactAlias"):
                listener.enterArtifactAlias(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitArtifactAlias"):
                listener.exitArtifactAlias(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitArtifactAlias"):
                return visitor.visitArtifactAlias(self)
            else:
                return visitor.visitChildren(self)

    class CreateArtifactContext(ArtifactContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ArtifactContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def STRING(self):
            return self.getToken(CoVeriLangParser.STRING, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterCreateArtifact"):
                listener.enterCreateArtifact(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitCreateArtifact"):
                listener.exitCreateArtifact(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitCreateArtifact"):
                return visitor.visitCreateArtifact(self)
            else:
                return visitor.visitChildren(self)

    def artifact(self):

        localctx = CoVeriLangParser.ArtifactContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_artifact)
        self._la = 0  # Token type
        try:
            self.state = 232
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input, 16, self._ctx)
            if la_ == 1:
                localctx = CoVeriLangParser.CreateArtifactContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 222
                self.match(CoVeriLangParser.T__22)
                self.state = 223
                self.artifact_type()
                self.state = 224
                self.match(CoVeriLangParser.T__5)
                self.state = 225
                _la = self._input.LA(1)
                if not (_la == CoVeriLangParser.ID or _la == CoVeriLangParser.STRING):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 226
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 2:
                localctx = CoVeriLangParser.ArtifactAliasContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 228
                self.match(CoVeriLangParser.ID)
                pass

            elif la_ == 3:
                localctx = CoVeriLangParser.ArtifactFromMapItemContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 229
                self.match(CoVeriLangParser.ID)
                self.state = 230
                self.match(CoVeriLangParser.T__23)
                self.state = 231
                self.match(CoVeriLangParser.ID)
                pass

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Artifact_typeContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TYPE_NAME(self):
            return self.getToken(CoVeriLangParser.TYPE_NAME, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_artifact_type

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterArtifact_type"):
                listener.enterArtifact_type(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitArtifact_type"):
                listener.exitArtifact_type(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitArtifact_type"):
                return visitor.visitArtifact_type(self)
            else:
                return visitor.visitChildren(self)

    def artifact_type(self):

        localctx = CoVeriLangParser.Artifact_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_artifact_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 234
            self.match(CoVeriLangParser.TYPE_NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Actor_typeContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TYPE_NAME(self):
            return self.getToken(CoVeriLangParser.TYPE_NAME, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_actor_type

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterActor_type"):
                listener.enterActor_type(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitActor_type"):
                listener.exitActor_type(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitActor_type"):
                return visitor.visitActor_type(self)
            else:
                return visitor.visitChildren(self)

    def actor_type(self):

        localctx = CoVeriLangParser.Actor_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_actor_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 236
            self.match(CoVeriLangParser.TYPE_NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_exp

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class ExpAliasContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterExpAlias"):
                listener.enterExpAlias(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitExpAlias"):
                listener.exitExpAlias(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitExpAlias"):
                return visitor.visitExpAlias(self)
            else:
                return visitor.visitChildren(self)

    class NotLogicalContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterNotLogical"):
                listener.enterNotLogical(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitNotLogical"):
                listener.exitNotLogical(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitNotLogical"):
                return visitor.visitNotLogical(self)
            else:
                return visitor.visitChildren(self)

    class InstanceOfContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterInstanceOf"):
                listener.enterInstanceOf(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitInstanceOf"):
                listener.exitInstanceOf(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitInstanceOf"):
                return visitor.visitInstanceOf(self)
            else:
                return visitor.visitChildren(self)

    class ElementOfContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def verdict_list(self):
            return self.getTypedRuleContext(CoVeriLangParser.Verdict_listContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterElementOf"):
                listener.enterElementOf(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitElementOf"):
                listener.exitElementOf(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitElementOf"):
                return visitor.visitElementOf(self)
            else:
                return visitor.visitChildren(self)

    class BinaryLogicalContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ExpContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ExpContext, i)

        def BIN_OP(self):
            return self.getToken(CoVeriLangParser.BIN_OP, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterBinaryLogical"):
                listener.enterBinaryLogical(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitBinaryLogical"):
                listener.exitBinaryLogical(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitBinaryLogical"):
                return visitor.visitBinaryLogical(self)
            else:
                return visitor.visitChildren(self)

    class ParenContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterParen"):
                listener.enterParen(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitParen"):
                listener.exitParen(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitParen"):
                return visitor.visitParen(self)
            else:
                return visitor.visitChildren(self)

    def exp(self, _p: int = 0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = CoVeriLangParser.ExpContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 36
        self.enterRecursionRule(localctx, 36, self.RULE_exp, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 262
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.T__24]:
                localctx = CoVeriLangParser.NotLogicalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 239
                self.match(CoVeriLangParser.T__24)
                self.state = 240
                self.exp(5)
                pass
            elif token in [CoVeriLangParser.T__25]:
                localctx = CoVeriLangParser.InstanceOfContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 241
                self.match(CoVeriLangParser.T__25)
                self.state = 242
                self.match(CoVeriLangParser.T__1)
                self.state = 243
                self.match(CoVeriLangParser.ID)
                self.state = 244
                self.match(CoVeriLangParser.T__5)
                self.state = 245
                self.artifact_type()
                self.state = 246
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__26]:
                localctx = CoVeriLangParser.ElementOfContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 248
                self.match(CoVeriLangParser.T__26)
                self.state = 249
                self.match(CoVeriLangParser.T__1)
                self.state = 250
                self.match(CoVeriLangParser.ID)
                self.state = 251
                self.match(CoVeriLangParser.T__5)
                self.state = 252
                self.match(CoVeriLangParser.T__3)
                self.state = 253
                self.verdict_list()
                self.state = 254
                self.match(CoVeriLangParser.T__4)
                self.state = 255
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.ID]:
                localctx = CoVeriLangParser.ExpAliasContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 257
                self.match(CoVeriLangParser.ID)
                pass
            elif token in [CoVeriLangParser.T__1]:
                localctx = CoVeriLangParser.ParenContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 258
                self.match(CoVeriLangParser.T__1)
                self.state = 259
                self.exp(0)
                self.state = 260
                self.match(CoVeriLangParser.T__2)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 269
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input, 18, self._ctx)
            while _alt != 2 and _alt != ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = CoVeriLangParser.BinaryLogicalContext(
                        self,
                        CoVeriLangParser.ExpContext(self, _parentctx, _parentState),
                    )
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp)
                    self.state = 264
                    if not self.precpred(self._ctx, 6):
                        from antlr4.error.Errors import FailedPredicateException

                        raise FailedPredicateException(
                            self, "self.precpred(self._ctx, 6)"
                        )
                    self.state = 265
                    self.match(CoVeriLangParser.BIN_OP)
                    self.state = 266
                    self.exp(7)
                self.state = 271
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input, 18, self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Verdict_listContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VERDICT(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.VERDICT)
            else:
                return self.getToken(CoVeriLangParser.VERDICT, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_verdict_list

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterVerdict_list"):
                listener.enterVerdict_list(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitVerdict_list"):
                listener.exitVerdict_list(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitVerdict_list"):
                return visitor.visitVerdict_list(self)
            else:
                return visitor.visitChildren(self)

    def verdict_list(self):

        localctx = CoVeriLangParser.Verdict_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_verdict_list)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 272
            self.match(CoVeriLangParser.VERDICT)
            self.state = 277
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == CoVeriLangParser.T__5:
                self.state = 273
                self.match(CoVeriLangParser.T__5)
                self.state = 274
                self.match(CoVeriLangParser.VERDICT)
                self.state = 279
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Tc_expContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_tc_exp

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterTc_exp"):
                listener.enterTc_exp(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitTc_exp"):
                listener.exitTc_exp(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitTc_exp"):
                return visitor.visitTc_exp(self)
            else:
                return visitor.visitChildren(self)

    def tc_exp(self):

        localctx = CoVeriLangParser.Tc_expContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_tc_exp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 280
            self.match(CoVeriLangParser.T__27)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Quoted_IDContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_quoted_ID

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterQuoted_ID"):
                listener.enterQuoted_ID(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitQuoted_ID"):
                listener.exitQuoted_ID(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitQuoted_ID"):
                return visitor.visitQuoted_ID(self)
            else:
                return visitor.visitChildren(self)

    def quoted_ID(self):

        localctx = CoVeriLangParser.Quoted_IDContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_quoted_ID)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 282
            self.match(CoVeriLangParser.T__28)
            self.state = 283
            self.match(CoVeriLangParser.ID)
            self.state = 284
            self.match(CoVeriLangParser.T__28)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    def sempred(self, localctx: RuleContext, ruleIndex: int, predIndex: int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[18] = self.exp_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def exp_sempred(self, localctx: ExpContext, predIndex: int):
        if predIndex == 0:
            return self.precpred(self._ctx, 6)
