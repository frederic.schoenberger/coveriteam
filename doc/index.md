<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

The introduction to CoVeriTeam can be divided in the following parts:
- [Installation](../README.md)
- Tool usage - described below
- [Language description](language.md)
- [Tutorial](../examples/README.md)
- Actor Definitions - still under construction

# Tool Usage
Run `bin/coveriteam --help` to see available command line arguments. 
The values required by the CoVeriTeam program are passed using the `--input` parameter followed by 
a `key=value` pair and the CoVeriTeam program file.

```bash
bin/coveriteam
    --input key1=val1                     (pass input parameters to the program)
    --input key2=val2                     (... and so on)
    --gen-code                            (print the generated python code instead of executing)
    --debug                               (print debug (and higher) messages from CoVeriTeam.)
    --debug-deep                          (print debug (and higher) messages from CoVeriTeam and also from dependent components)
    <program_file>                        (CoVeriTest program)
    --cache-dir <CACHE_DIR>               (Explicitly set the path to the cache.)
    --remote                              (Execute CoVeriTeam remotely.)
    --tool-info                           (Test the given YML file configuration by executing the tool for version.)
    --clean                               (Delete cache before execution)
    --data-model <DATA_MODEL>             (Data model of the program under analysis. 
                                           Please choose ILP32 for 32 bit architecture, and LP64 for 64 bit. Default value is ILP32.)
```

On successful execution a uniquely named folder is created in the folder `cvt-output`.
This folder contains 1) the artifacts produced during the execution of the actor, 
and 2) an xml file named `execution_trace.xml` containing the execution trace of the composition,
i.e., the resource measurements for atomic actors and paths to the artifacts produced.

In addition to this, a symbolic link named `lastexecution` pointing to the directory containing the artifacts
produced in the latest execution is also created.

### Atomic Actors
CoVeriTeam is based on using off-the-shelf verification and testing tools for cooperative verification
as atomic actors.
On its first execution, CoVeriTeam automatically downloads and unzips the archives that contain
the atomic actors used in the CoVeriTeam program.
The source URL is specified in the YAML atomic-actor definition.
The atomic actors that we use in the tutorial examples are available in the [actors/](actors/) folder.

`bin/coveriteam --tool-info <YAML actor-definition file>`
prints information (including name and version) about the actor defined in the YAML file.
This can be used to test if the atomic actor was successfully installed.
The script `smoke_test_all_tools.sh` prints the tool info for each actor defined in the `actors/` folder.